OG Rebuilder
--------------------------
The OG Rebuilder module rebuilds the group node permissions when the group access
is changed.

Credits / Contact
--------------------------------------------------------------------------------
Currently maintained by Diogo Correia [1].

Sponsored by DRI — Discovery / Reinvention / Integration [2]

References
--------------------------------------------------------------------------------
1: http://drupal.org/user/887060
2: http://dri-global.com
